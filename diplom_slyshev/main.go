package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/SevereCloud/vksdk/v2/api"
)

// 198646308
var groups = make(map[int]struct{})

func main() {
	timeNow := time.Now()
	token := "vk1.a.dsadiugkozH7sfM4_A3YuEr5TBZtocmLjC-RUJPhaCK5nWTEaOFXcI30Tg-wLxPVd8N0WjfkdWLTjXlIsvT-v2weCf9Ud73O4ylUZ4_2jm9LLjY3z5koxy9O5FIHpSivVmK165Oh7mJcF85alke1nExPQV3pwcqVG7ttdFjg1e1folrCiZMdw5ZUtsEngj5w-HeW7V-uz65_eW85Yu2dsQ"
	vk := api.NewVK(token)
	var (
		id int
	)

	fmt.Print("Введи ID группы: \n")
	fmt.Fscan(os.Stdin, &id)

	// идем за записями группы-сабскрайберы/пользователь-друзья/пользователь-группы
	groupSubscribers, err := GetMembers(vk, id)
	if err != nil {
		log.Fatal(err)
	}

	for _, userID := range groupSubscribers {
		err := GetGroups(vk, userID)
		if err != nil {
			log.Println("HERE")
			log.Fatal(err)
		}
		time.Sleep(100 * time.Millisecond)
	}

	groupsIDs := make([]int, 0, len(groups))
	for key := range groups {
		groupsIDs = append(groupsIDs, key)
	}

	csvFile, err := os.Create("./vector.csv")
	defer csvFile.Close()
	if err != nil {
		log.Fatal(err)
	}

	w := csv.NewWriter(csvFile)
	strings := []string{"id", "num_of_friends"}
	for _, id := range groupsIDs {
		strings = append(strings, strconv.Itoa(id))
	}

	records := make([][]string, 0)
	for _, userID := range groupSubscribers {
		record, _ := GetFriendsCountAndGroups(vk, userID, groupsIDs)
		records = append(records, record)
		time.Sleep(500 * time.Millisecond)
	}

	w.Write(strings)
	w.WriteAll(records)
	log.Println(timeNow.Sub(time.Now()) * -1)
}

func GetFriendsCountAndGroups(vk *api.VK, id int, groupsIDs []int) ([]string, error) {
	var (
		groupsStringIDs = make([]string, 0, len(groupsIDs))
		count           = 0
		wg              = sync.WaitGroup{}
	)
	wg.Add(1)
	go func() {
		var (
			offset = 0
			i      = 0
		)
		defer wg.Done()
		for {
			log.Println("in progress by user friend: ", id)
			res, err := vk.FriendsGet(api.Params{
				"user_id": id,
				"offset":  offset,
			})
			if err != nil && (err.Error() == "api: This profile is private" || err.Error() == "api: User was deleted or banned") {
				break
			} else if err != nil {
				log.Fatal(err)
			}

			count += res.Count

			if i == res.Count/1000 {
				break
			}
			offset += 1000
			i++
			time.Sleep(500 * time.Millisecond)
		}
	}()

	wg.Add(1)
	go func() {
		var (
			offset = 0
			i      = 0
		)
		defer wg.Done()
		for {
			log.Println("in progress by user groups: ", id)
			res, err := vk.UsersGetSubscriptions(api.Params{
				"user_id": id,
				"offset":  offset,
			})
			if err != nil && (err.Error() == "api: This profile is private" || err.Error() == "api: User was deleted or banned") {
				break
			} else if err != nil {
				log.Fatal(err)
			}

			mapGroups := make(map[int]struct{})
			for _, groupID := range res.Groups.Items {
				mapGroups[groupID] = struct{}{}
			}

			for _, groupID := range groupsIDs {
				if _, ok := mapGroups[groupID]; ok {
					groupsStringIDs = append(groupsStringIDs, "1")
				} else {
					groupsStringIDs = append(groupsStringIDs, "0")
				}
			}

			if i == res.Groups.Count/1000 {
				break
			}
			offset += 1000
			i++
			time.Sleep(500 * time.Millisecond)
		}
	}()
	wg.Wait()

	result := make([]string, 0)
	result = append(result, strconv.Itoa(id), strconv.Itoa(count))
	result = append(result, groupsStringIDs...)

	return result, nil
}

func GetGroups(vk *api.VK, id int) error {
	var (
		offset = 0
		i      = 0
	)
	for {
		log.Println("in progress by group: ", id)
		res, err := vk.UsersGetSubscriptions(api.Params{
			"user_id": id,
			"offset":  offset,
		})
		if err != nil {
			if err.Error() == "api: This profile is private" || err.Error() == "api: User was deleted or banned" {
				break
			}
			return err
		}

		if res.Groups.Items == nil {
			break
		}

		for _, groupID := range res.Groups.Items {
			groups[groupID] = struct{}{}
		}

		if i == res.Groups.Count/1000 {
			break
		}
		offset += 1000
		i++
		time.Sleep(100 * time.Millisecond)
	}

	return nil
}

func GetMembers(vk *api.VK, id int) ([]int, error) {
	res, err := vk.GroupsGetMembers(api.Params{
		"group_id": id,
	})
	if err != nil {
		return nil, err
	}

	return res.Items, nil
}
