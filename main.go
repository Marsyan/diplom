package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/SevereCloud/vksdk/v2/api"
)

type ResultIDs struct {
	Items []int
	Count int
}

func main() {
	timeNow := time.Now()
	token := "vk1.a.dsadiugkozH7sfM4_A3YuEr5TBZtocmLjC-RUJPhaCK5nWTEaOFXcI30Tg-wLxPVd8N0WjfkdWLTjXlIsvT-v2weCf9Ud73O4ylUZ4_2jm9LLjY3z5koxy9O5FIHpSivVmK165Oh7mJcF85alke1nExPQV3pwcqVG7ttdFjg1e1folrCiZMdw5ZUtsEngj5w-HeW7V-uz65_eW85Yu2dsQ"
	vk := api.NewVK(token)
	log.Println(vk)
	var (
		choose int
		id     int
	)
	fmt.Print("Выбери число (1/2/3): \n1. ID группы —> ID подписчиков\n2. ID пользователя —> ID друзей\n3. ID пользователя —> ID групп\n")
	fmt.Fscan(os.Stdin, &choose)

	fmt.Print("Введи ID: \n")
	fmt.Fscan(os.Stdin, &id)

	// идем за записями группы-сабскрайберы/пользователь-друзья/пользователь-группы
	subscribers, records := GetInfo(vk, id, choose)

	csvFile, err := os.Create("./file1.csv")
	if err != nil {
		log.Fatal(err)
	}

	w := csv.NewWriter(csvFile)
	w.Write([]string{"id", "idSub"})
	w.WriteAll(records)

	// если выбрали не группу/человека, дальше идти смысла нет, csv уже записан
	if choose != 1 && choose != 2 || len(subscribers) == 0 {
		return
	}

	friends := GetFriendsInOneGroup(vk, subscribers)
	csvFile2, err := os.Create("./file2.csv")
	if err != nil {
		log.Fatal(err)
	}
	w2 := csv.NewWriter(csvFile2)
	w2.Write([]string{"id", "idSub"})

	recordsRes := make([][]string, 0)
	for key, v := range friends {
		for _, str := range v {
			recordsRes = append(recordsRes, []string{key, str})
		}
	}

	w2.WriteAll(recordsRes)
	log.Println(timeNow.Sub(time.Now()) * -1)
}

func GetFriendsInOneGroup(vk *api.VK, subs map[int][]int) map[string][]string {
	var (
		offset = 0
		i      = 0
		total  = 0
		result = make(map[string][]string, 0)
		wg     sync.WaitGroup
		mx     sync.Mutex
	)

	for id, subGroup := range subs {
		wg.Add(1)
		time.Sleep(50 * time.Millisecond)
		go func(id, i, offset int, subGroup []int) {
			for {
				total++
				log.Println("in progress id: ", id)
				res, err := vk.FriendsGet(api.Params{
					"user_id": id,
					"offset":  offset,
				})
				if err != nil && (err.Error() == "api: This profile is private" || err.Error() == "api: User was deleted or banned") {
					break
				} else if err != nil {
					log.Fatal(err)
				}

				for _, memberID := range res.Items {
					for _, member := range subGroup {
						if id == member {
							continue
						}

						if member == memberID {
							mx.Lock()
							result[strconv.Itoa(id)] = append(result[strconv.Itoa(id)], strconv.Itoa(member))
							mx.Unlock()
						}
					}
				}
				if i == res.Count/1000 {
					break
				}
				offset += 1000
				i++
			}
			defer wg.Done()
		}(id, i, offset, subGroup)
	}

	wg.Wait()

	return result
}

func GetInfo(vk *api.VK, id int, choose int) (map[int][]int, [][]string) {
	var (
		offset      = 0
		i           = 0
		total       = 0
		subscribers = make(map[int][]int, 0)
		records     = make([][]string, 0)
	)

	for {
		log.Println("in progress: ", offset)
		res, err := chooseHandler(id, offset, choose, vk)
		if err != nil {
			log.Fatal(err)
		}

		record := make([]string, 0)
		for _, memberID := range res.Items {
			subscribers[memberID] = append(subscribers[memberID], res.Items...)
			record = []string{strconv.Itoa(id), strconv.Itoa(memberID)}
			records = append(records, record)
			total++
		}
		if i == res.Count/1000 {
			break
		}
		offset += 1000
		i++
		time.Sleep(500 * time.Millisecond)
	}

	log.Println("total: ", total)

	return subscribers, records
}

func chooseHandler(id, offset, choose int, vk *api.VK) (*ResultIDs, error) {
	resultIDs := &ResultIDs{}
	switch choose {
	case 1:
		res, err := vk.GroupsGetMembers(api.Params{
			"group_id": id,
			"offset":   offset,
		})
		if err != nil {
			return nil, err
		}
		resultIDs.Count = res.Count
		resultIDs.Items = append(resultIDs.Items, res.Items...)
		return resultIDs, err
	case 2:
		res, err := vk.FriendsGet(api.Params{
			"user_id": id,
			"offset":  offset,
		})
		if err != nil {
			return nil, err
		}
		resultIDs.Count = res.Count
		resultIDs.Items = append(resultIDs.Items, res.Items...)
		return resultIDs, err
	case 3:
		res, err := vk.UsersGetSubscriptions(api.Params{
			"user_id": id,
			"offset":  offset,
		})
		if err != nil {
			return nil, err
		}
		resultIDs.Count = res.Groups.Count
		resultIDs.Items = append(resultIDs.Items, res.Groups.Items...)
		return resultIDs, err
	}
	return &ResultIDs{}, nil
}

func GroupsGetMembers(vk *api.VK, csvFile *os.File, id int) {
	var (
		offset      = 0
		i           = 0
		total       = 0
		subscribers = make([]int, 0)
		w           = csv.NewWriter(csvFile)
	)

	w.Write([]string{"groupID", "memberID"})
	for {
		log.Println("in progress: ", offset)
		res, err := vk.GroupsGetMembers(api.Params{
			"group_id": id,
			"offset":   offset,
		})
		if err != nil {
			log.Fatal(err)
		}

		record := make([]string, 0)
		records := make([][]string, 0)
		for _, memberID := range res.Items {
			subscribers = append(subscribers, memberID)
			record = []string{"public" + strconv.Itoa(id), "id" + strconv.Itoa(memberID)}
			records = append(records, record)
			total++
		}
		w.WriteAll(records)
		if i == res.Count/1000 {
			break
		}
		offset += 1000
		i++
		time.Sleep(500 * time.Millisecond)
	}

	log.Println("total: ", total)
}

func FriendsGet(vk *api.VK, csvFile *os.File, id int) {
	var (
		offset = 0
		i      = 0
		total  = 0
		w      = csv.NewWriter(csvFile)
	)

	w.Write([]string{"ID", "getID"})
	for {
		log.Println("in progress: ", offset)
		res, err := vk.FriendsGet(api.Params{
			"user_id": id,
			"offset":  offset,
		})
		if err != nil {
			log.Fatal(err)
		}

		record := make([]string, 0)
		records := make([][]string, 0)
		for _, memberID := range res.Items {
			record = []string{"id" + strconv.Itoa(id), "id" + strconv.Itoa(memberID)}
			records = append(records, record)
			total++
		}
		w.WriteAll(records)
		if i == res.Count/1000 {
			break
		}
		offset += 1000
		i++
		time.Sleep(500 * time.Millisecond)
	}

	log.Println("total: ", total)
}

func UsersGetSubscriptions(vk *api.VK, csvFile *os.File, id int) {
	var (
		offset = 0
		i      = 0
		total  = 0
		w      = csv.NewWriter(csvFile)
	)

	w.Write([]string{"ID", "getID"})
	for {
		log.Println("in progress: ", offset)
		res, err := vk.UsersGetSubscriptions(api.Params{
			"user_id": id,
			"offset":  offset,
		})
		if err != nil {
			log.Fatal(err)
		}

		record := make([]string, 0)
		records := make([][]string, 0)
		for _, memberID := range res.Groups.Items {
			record = []string{"id" + strconv.Itoa(id), "public" + strconv.Itoa(memberID)}
			records = append(records, record)
			total++
		}
		w.WriteAll(records)
		if i == res.Groups.Count/1000 {
			break
		}
		offset += 1000
		i++
		time.Sleep(500 * time.Millisecond)
	}

	log.Println("total: ", total)
}
