package main

import (
	"log"
	"strconv"
	"strings"
)

func main() {

}

func test(s string) {
	q := strings.Split(s, ",")
	log.Println(len(q))
	for _, n := range q {
		_, err := strconv.Atoi(n)
		if err != nil {
			panic(err)
		}
	}
}
